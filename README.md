# Views HTML5 Style

Drupal 7 did a nice job of incorpoarting most of the [Semantic Views](http://drupal.org/project/semanticviews) project with one minor oversight. The ability to change the wrapping element of each view rows. That leaves each row usually wrapped with pre-HTML5 elements.

## Use Case

There was a need to wrap each row with an <article> tag. This can be done within other view settings, but the result would still be wrapped in a row <div>.

## Solution

This module adds a replacement style for the **unformatted** style. Selecting the **HTML5 Rows** style instead lets a user select from a predetermined list of HTML and HTML5 tags to use as the row element.

# Installation

2. Add the module into the modules directory *(sites/all/modules)*.
4. Enable the modules from the modules admin page *(admin/modules)*.
1. Add or Edit a view (admin/structure/views)
    * Change the **Format** to **HTML5 Rows**.
    * In the settings of *HTML5 Rows*, choose the HTML elemnt that you would like to be the view row element.

### _Future Improvement Brain Storming_
1. Maybe change the input from a select to a textfield to allow an admin to type in their own element instead of dictating it to them.
    * Demand will warrant that change.
2. Add a field style
    * No *valid* use case in mind yet