<?php
/**
 * Implements hook_views_plugins().
 */
function views_row_html5_views_plugins() {
  return array(
    'style' => array(
      'html5' => array(
        'title' => t('HTML5 Rows'),
        'type' => 'normal',
        'path' => drupal_get_path('module', 'views_row_html5') . '/views',
        'handler' => 'views_plugin_style_html5',
        'uses fields' => TRUE,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'theme' => 'views_view_html5',
        'theme path' => drupal_get_path('module', 'views_row_html5') .  
          '/views',
        'theme file' => 'html5.views.theme.inc',
        'help' => t('Render a view using any available HTML / HTML5 tag as the row wrapper.')
      )
    )
  );
}
