<?php

/**
 * @file
 * Default simple view template to display a list of rows using the selected HTML element.
 *
 * @ingroup views_templates
 *
 * $html_row = the selected HTML element within the view style settings form
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php if($html_row == 'li'): // if $html_row = 'li', wrap with a <ul> ?>
<ul<?php if($list_style != 'none'): print ' class="' . $list_style . '"'; endif; ?>>
<?php endif; ?>

<?php foreach ($rows as $id => $row): ?>
  <<?php print $html_row; ?><?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </<?php print $html_row; ?>>
<?php endforeach; ?>

<?php if($html_row == 'li'): // if $html_row = 'li', wrap with a <ul> ?>
</ul>
<?php endif; ?>