<?php
/**
 * Style plugin to render each item in a definition list.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_html5 extends views_plugin_style {
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);


    // Create an array of available HTML5 tags to use of the view row element
    $row_tags = drupal_map_assoc(array('article','aside','section','div','li'));

    $form['html_row'] = array(
      '#type' => 'select',
      '#title' => t('Select an HTML element to wrap each row'),
      '#options' => $row_tags,
      '#default_value' =>  (isset($this->options['html_row']) ? $this->options['html_row'] : $row_tags['div']),
    );

    // Create an array of list styles
    $list_styles = drupal_map_assoc(array('none','inline'));
 
    $form['list_style'] = array(
      '#type' => 'select',
      '#title' => t('Select a list style'),
      // The prefix/suffix provide the div that we're replacing, named by
      // #ajax['wrapper'] above.
      '#prefix' => '<div id="list-style-div">',
      '#suffix' => '</div>',
      '#options' => $list_styles,
      '#default_value' => $this->options['list_style'],
      '#description' => t('Should the list be the <b>standard</b> stacked, or <b>inline</b>?'),
      '#states' => array(
        'visible' => array(   // action to take.
          ':input[name="style_options[html_row]"]' => array('value' => 'li'),
        ),
      )
    );
    // dpm(array($form,$form_state));
  }
}

// DEFAULT values are appearing eventhough values have been saved.