<?php
/**
 * Make variables available to the definition list template.
 * file.
 */
function template_preprocess_views_view_html5(&$vars) {
  template_preprocess_views_view_unformatted($vars);
  // set the $html_row element
  $vars['html_row'] = $vars['options']['html_row'];
  if(isset($vars['options']['list_style'])){
	  $vars['list_style'] = $vars['options']['list_style'];
  }
}